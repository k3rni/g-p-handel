from typing import TextIO
import string
from unicodedata import name as uname

from .layouts import Layout

class KeybindingsPlist:
    def __init__(self, out: TextIO, layout: Layout, leader_key: str):
        self.leader_key = leader_key
        self.layout = layout
        self.out = out

    def pr(self, msg, indent=0):
        lead = " " * 4 * indent
        self.out.write(f"{lead}{msg}\n")

    def convert(self, tree: dict):
        # Move subtrees that don't start with <Multi_key> under it.
        # This handles the <dead_greek> and <dead_currency> groups.
        root = tree['<Multi_key>']
        for key in tree:
            if key == '<Multi_key>':
                continue

            root[key] = tree[key]

        # Plist handles nesting already. All we need to do is produce proper format on output
        self.pr("{")
        self.convert_section("<Multi_key>", root, 1)
        self.pr("}")

    def map_key(self, key: str) -> str:
        if key == '<Multi_key>':
            return self.leader_key

        # Some keys may need to be unicode-escaped?
        return self.layout.keycode(key)

    def unicode_escape(self, key):
        # Unused, but may be required by some values.
        return ''.join(f"\\U{ord(ch):04X}" for ch in key)

    def convert_section(self, leader: str, section: dict, level: int):
        # Convenience bindings
        pr = self.pr
        map_key = self.map_key
        esc = self.unicode_escape
        # Given a (k, v) pair, return last symbol of k
        last_letter = lambda key: key[0][-1]

        if leader != map_key(leader):
            # E.g. any shifted key. This will show the original symbol in a comment
            info = f' // {leader}'
        else:
            info = ''

        pr(f'"{map_key(leader)}" = ' + '{' + info, level)
        for symbol, value in sorted(section.items(), key=last_letter):
            key = symbol[1:-1]
            if isinstance(value, dict):
                self.convert_section(key, value, level + 1)
            else:
                try:
                    # Usually we can get the character's name
                    name = uname(value)
                except TypeError:
                    name = ''

                comment = f" // {key} => {value!r} {name}"
                directive = f'"{map_key(key)}" = ("insertText:", "{value}");'
                pr(directive + comment, level + 1)
        pr('};', level)

