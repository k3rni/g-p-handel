#! python3
# encoding: utf-8

import argparse
import sys

from converter import Converter, available_layouts, available_blocks

def option_parser():
    layouts = available_layouts()
    blocks = available_blocks()
    parser = argparse.ArgumentParser(description="Convert a X11 Compose file to MacOS keybindings plist. Writes to stdout.",
                                     epilog=f"""
                                     Available layouts: {','.join(layouts)}.
                                     Available blocks: {','.join(blocks)}.
                                     """)
    parser.add_argument('input_file', type=argparse.FileType('r', encoding='utf-8'),
                        help="A file in XKB Compose format. Usually named Compose or Compose.pre")
    parser.add_argument('-c', '--compose-key', type=str, default='F24', required=False)

    parser.add_argument('-L', '--layout', dest='layout', metavar='LAYOUT', type=str, required=False, choices=layouts, default=layouts[0],
                        help="Keyboard layout (important for some symbols)")

    parser.add_argument('-i', '--include', dest='include_blocks', metavar='BLOCK', type=str, required=False, action='extend', nargs='+', choices=blocks,
                        help="""Symbol groups to include, defaults to all. Can provide more than once to include all named groups.
                        However, some groups may require keys from a specific layout.
                        """)

    parser.add_argument('-x', '--exclude', dest='exclude_blocks', metavar='BLOCK', type=str, required=False, action='extend', nargs='+', choices=blocks,
                        help="""Symbol groups to exclude. Can provide more than once to exclude all the named groups.
                        """)

    return parser

if __name__ == "__main__":
    parser = option_parser()
    args = parser.parse_args()
    args.include_blocks = set(args.include_blocks if args.include_blocks else ['all'])
    args.exclude_blocks = set(args.exclude_blocks if args.exclude_blocks else [])

    converter = Converter(args.layout, args.include_blocks, args.exclude_blocks, args.compose_key)
    converter.convert(args.input_file, sys.stdout)

