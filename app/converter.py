from typing import TextIO, Sequence, Iterable, Callable
from importlib import import_module
from pprint import pprint

from .layouts import Layout
from .compose_parser import ComposeParser, ComposeDirective
from .tree import TreeBuilder
from .plist import *
from .cocoa import COCOA_KEYS

def available_blocks() -> Sequence[str]:
    filters = import_module('app.filters')

    return [name[7:] for name in dir(filters)
            if name.startswith('filter_')]

def available_layouts() -> Sequence[str]:
    layouts = import_module('app.layouts')
    return [name
            for name, obj in vars(layouts).items()
            if isinstance(obj, type) and # Is a class
               issubclass(obj, layouts.Layout) and # Inheriting Layout
               obj != layouts.Layout
            ]

def lookup_layout(layout: str) -> Layout:
    layouts = import_module('app.layouts')
    kls = vars(layouts)[layout]
    return kls()

FilterFunction = Callable[[ComposeDirective], bool]

def select_filters(names: Iterable[str]) -> list[FilterFunction]:
    filters = import_module('app.filters')
    return [func
            for name, func in vars(filters).items()
            if name.startswith("filter_") and name[7:] in names]

def build_filter(include_blocks: Iterable[str], exclude_blocks: Iterable[str]) -> FilterFunction:
    include_functions = select_filters(include_blocks)
    exclude_functions = select_filters(exclude_blocks)

    def combo_filter(expr: ComposeDirective) -> bool:
        if any(callable(expr) for callable in exclude_functions):
            return False

        return any(callable(expr) for callable in include_functions)

    return combo_filter

def lookup_compose_key(key: str) -> str:
    code = COCOA_KEYS[key]
    return f'\\U{code:=04X}'

class Converter:
    def __init__(self, layout: str, include_blocks: set[str], exclude_blocks: set[str], compose_key: str):
        self.layout = lookup_layout(layout)
        self.symbol_filter = build_filter(include_blocks, exclude_blocks)
        self.leader_key = lookup_compose_key(compose_key)

    def convert(self, io: TextIO, out: TextIO):
        directives = list(ComposeParser().load(io))
        filtered_directives = [d for d in directives if self.symbol_filter(d)]
        typeable_directives = [d for d in filtered_directives
                               if self.valid_keys(d.inputs)]
        graph = TreeBuilder(typeable_directives).graph
        KeybindingsPlist(out, self.layout, self.leader_key).convert(graph)

    def valid_keys(self, inputs: Sequence[str]) -> bool:
        return all(self.layout.valid_key(key) for key in inputs)
