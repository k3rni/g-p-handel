'''
Sources:
- https://developer.apple.com/library/archive/technotes/tn2450/_index.html
- https://www.usb.org/sites/default/files/documents/hut1_12v2.pdf
'''
HID_KEYS = {
    # These two are absent on US keyboards, and make good virtual keys
    'non_us_backslash': 0x64, 'non_us_hash_tilde': 0x32,
    # Good choices for the Compose key
    'caps_lock': 0x39,
    'left_ctrl': 0xe0, 'left_shift': 0xe1, 'left_alt': 0xe2, 'left_gui': 0xe3,
    'right_ctrl': 0xe4, 'right_shift': 0xe5, 'right_alt': 0xe6, 'right_gui': 0xe7,
    # The 'menu' key
    'application': 0x65,
    # Listed in the Apple Technote, and also have corresponding mappings in the bindable private Unicode range.
    # However, they may not be directly usable.
    'F13': 0x68, 'F14': 0x69, 'F15': 0x6a, 'F16': 0x6b, 'F17': 0x6c, 'F18': 0x6d,
    'F19': 0x6e, 'F20': 0x6F, 'F21': 0x70, 'F22': 0x71, 'F23': 0x72, 'F24': 0x73,
    # Other candidates
    'ScrollLock': 0x47, # On M1 2020 emits "brightness down"
    'Pause': 0x48, # brightness up
    # The USB spec lists more keys, but we're limited to the set that also has mappings in the Unicode range.
    # The technote however, stops at 0xE7.
    # Following keys are still listed in the Unicode range (compare in cocoa.py)
    'SysReq':  0x9a,
}

HID_MASK = 0x700000000
