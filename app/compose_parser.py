from dataclasses import dataclass
import re
from typing import Optional, Sequence
import typing
from ast import literal_eval

@dataclass(order=True)
class ComposeDirective:
    # <Multi_key> <apostrophe> <E>
    inputs: Sequence[str]
    # "́É", possibly with escape sequences
    symbol: str
    # Eacute
    entity: Optional[str] = None
    # LATIN CAPITAL LETTER E WITH ACUTE
    description: Optional[str] = None

class ComposeParser:
    def __init__(self):
        pass

    def load(self, io: typing.TextIO):
        for line in io.readlines():
            if line.startswith("#") or line.startswith("XCOMM"):
                continue

            yield self.parse_line(line)


    XCOMP_RE = re.compile(r'''^
        ((?:<\w+>\s*)+)\s* # inputs
        (?:\s+:\s+)        # separating colon
        ("[^ \t]+)         # symbol, can contain escapes so matched till next space or tab
        (?:\s+(\w+))?      # entity, optional
        (?:\s+\#\s+(.*))?  # description till eol
    $''', re.VERBOSE)

    def parse_line(self, line: str) -> Optional[ComposeDirective]:
        if match := self.XCOMP_RE.match(line):
            inputs, symbol, entity, descr = match.groups()
            return ComposeDirective(tuple(inputs.strip().split()), literal_eval(symbol), entity, descr)

        print(line)
        return None
