# encoding: utf-8

from typing import TextIO

from .hid_keys import HID_KEYS, HID_MASK

# No need to employ proper xml generators, because the content is small and fixed.
REMAP_CONTENT = '''
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>Label</key>
        <string>{label}</string>
        <key>ProgramArguments</key>
        <array>
            <string>/usr/bin/hidutil</string>
            <string>property</string>
            <string>--set</string>
            <string>{key_mapping}</string>
        </array>
        <key>RunAtLoad</key>
        <true/>
    </dict>
</plist>
'''


# Could use JSON but want to have hexadecimal integers. Double braces because it's a format string.
SINGLE_MAPPING = '''
{{ "HIDKeyboardModifierMappingSrc": {:#0x},
   "HIDKeyboardModifierMappingDst": {:#0x} 
}}
'''.replace("\n", "")

def single_mapping(key_from: str, key_to: str) -> str:
    return SINGLE_MAPPING.format(
        HID_MASK | HID_KEYS[key_from],
        HID_MASK | HID_KEYS[key_to]
    )

class Remap:
    def __init__(self, compose_key: str, prefix_key: str):
        self.compose_key = compose_key
        self.prefix_key = prefix_key

    def remap(self, out: TextIO):
        out.write(
            REMAP_CONTENT.format(
                label = "dev.local.keyRemapper",
                key_mapping=self.user_key_mapping()
            )
        )

    def user_key_mapping(self):
        # Doubled braces because this is a format string
        return '''{{"UserKeyMapping":[
            {}
        ]}}'''.format(single_mapping(self.compose_key, self.prefix_key))
