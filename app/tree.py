from typing import Iterable, Any
from itertools import groupby

from .compose_parser import ComposeDirective

class TreeBuilder(object):
    def __init__(self, directives: Iterable[ComposeDirective]):
        """
        Root
        |- <Multi_key>
           |- <parenleft>
           |  |- <1>
           |  |  |- <parenright> => Term("①")
           |  |  |- <0>
           |  |     |- <parenright> => Term("⑩")
           |  |- <K>
           |  |  |- <parenright> => Term("Ⓐ")
        """
        self.graph = self.build(directives, level=1)

    def build(self, directives: Iterable[ComposeDirective], level: int) -> dict[str, Any]:
        result: dict[str, Any] = {}
        # At level n, group by (n-1)'th key in each directive
        for lead, grouped_rules in groupby(directives, lambda cd: cd.inputs[level - 1]):
            rules = list(grouped_rules)
            # Directives with exactly one more symbol are terminals
            terms = [cd for cd in rules if len(cd.inputs) == level + 1]
            finals = { term.inputs[-1]: term.symbol for term in terms }

            # Directives with more symbols left will be parsed further into subtrees
            subs = [cd for cd in rules if len(cd.inputs) > level + 1]

            result[lead] = (
                result.get(lead, {}) |      # Merge with any previous definitions
                finals |                    # Adding terminal symbols
                self.build(subs, level + 1) # and recursively built subtrees
            )

        return result
