from typing import Callable
from functools import partial
from unicodeblock import blocks as ublocks # type: ignore

from .compose_parser import ComposeDirective as CD

FilterMethod = Callable[[CD], bool]

def _filter_by_blocks(block_names: list[str], directive: CD):
    """Helper function used by block_filter() to construct filters.
    Don't use directly."""
    try:
        block = ublocks.of(directive.symbol)
        return block in block_names
    except TypeError:
        return False

def _filter_by_desc(matcher: str, directive: CD):
    desc = directive.description
    if not desc:
        return False
    return matcher in desc

def block_filter(*block_names: str) -> FilterMethod:
    return partial(_filter_by_blocks, block_names)

def desc_filter(matcher):
    return partial(_filter_by_desc, matcher)

def filter_all(directive: CD):
    return True

filter_greek = desc_filter('GREEK')
filter_circled = desc_filter('CIRCLED')

filter_currency = block_filter('CURRENCY_SYMBOLS')
filter_math = block_filter('MATHEMATICAL_OPERATORS', 'MISCELLANEOUS_TECHNICAL')
filter_punctuation = block_filter('GENERAL_PUNCTUATION', 'BASIC_PUNCTUATION')

_filter_latin_plain = desc_filter('LATIN')
def filter_latin(directive: CD) -> bool:
    # Exclude circled which have descriptions like CIRCLED LATIN CAPITAL LETTER X
    if filter_circled(directive):
        return False

    return _filter_latin_plain(directive)

def filter_superscripts_and_subscripts(directive: CD) -> bool:
    # Special cases that are in other blocks (legacy)
    if directive.symbol in '¹²³™℠':
        return True

    return block_filter('SUPERSCRIPTS_AND_SUBSCRIPTS', 'SPACING_MODIFIER_LETTERS')(directive)

def filter_fractions(directive: CD) -> bool:
    # Special cases that are in other blocks (legacy)
    if directive.symbol in '½¼¾':
        return True

    return block_filter('NUMBER_FORMS')(directive)
