import string

class Layout:
    NAMED_KEYS: dict[str, str] = {}

    def keycode(self, symbol: str) -> str:
        return self.NAMED_KEYS.get(symbol, symbol)

    def valid_key(self, keycode: str) -> bool:
        if keycode == '<Multi_key>':
            return True

        symbol = keycode[1:-1] # strip angle brackets
        if symbol in self.NAMED_KEYS:
            return True

        return False

    def valid_ascii(self, keycode: str) -> bool:
        symbol = keycode[1:-1]

        if symbol in string.ascii_letters:
            return True
        if symbol in string.digits:
            return True

        return False


class US(Layout):
    NAMED_KEYS = {
        # Keys are whatever can appear in angle brackets in the Compose file,
        # and values are NSCocoa notations for actions to produce that symbol
        # For example, on a US keyboard the ampersand sign & is on the key 7, and requires pressing shift ($)
        # Sorted alphabetically by name.
        'ampersand': "$7",
        'apostrophe': "'",
        'asciicircum': "$6",
        'asterisk': "$8",
        'asciitilde': "$`",
        'backslash': r'\\', # Emits double-backslash in output
        'bar': r"$\\",
        'colon': "$;",
        'comma': ",",
        'equal': "=",
        'exclam': "$1",
        'grave': "`",
        'greater': "$.",
        'less': "$,",
        'minus': "-",
        'numbersign': "$3",
        'parenleft': "$9",
        'parenright': "$0",
        'percent': "$5",
        'period': ".",
        'plus': "$-",
        'question': "$/",
        'quotedbl': "$'",
        'semicolon': ';',
        'slash': '/',
        'space': " ",
        'underscore': "$-",

        # Special: Map some <dead_xxx> to something, so that we can more symbols.
        'dead_currency': '$4', # Arbitrary choice: dollar sign
        'dead_greek': r'\\', # Arbitrary: backslash, a poor man's lowercase lambda λ
    }

    def valid_key(self, keycode: str) -> bool:
        return super().valid_key(keycode) or self.valid_ascii(keycode)
