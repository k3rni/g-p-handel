<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="{{ url_for('static', filename='css/pico.min.css') }}" />
    <style type="text/css">
        nowrap {
            white-space: nowrap;
        }
        nav.right {
            display: flex;
            justify-content: flex-end;
        }
    </style>
</head>
<body>
    <main class="container" v-scope @vue:mounted="mounted">
        <section>
            <h1>What is this?</h1>
            <nav class="right">
                <a href="#generate-compose">Skip explanation</a>
            </nav>
            <p>If you are a Linux user, you may be familiar with the power of the Compose key. Briefly, it enables you to type
                many characters not immediately accessible on your keyboard with a short, memorable sequence.</p>

            <p>For example to type <code>ŏ</code>, which is a lowercase letter O with a <a href="https://en.wikipedia.org/wiki/Breve">breve sign</a>, the sequence is
                {{ comp }} then {{ "b"|kbd }} (for <em>breve</em>) then {{ "o"|kbd }}.
                On current (2022) MacOS, you may be able to type it by holding down the {{ "o"|kbd }} key until a palette of variants appears,
                then pressing a number key to select a variant. The <code>ŏ</code> may or may not be in this list, depending on your
                language settings.</p>
            <p>This is problematic for power users. First, it restricts your choice based on current locale. Second, it <strong>breaks key repeat</strong> for certain (letter) keys.</p>
            <p>For a list of available sequences and characters, consult the <code>Compose</code> file in freedesktop.org's <a href="https://github.com/freedesktop/xorg-libX11/blob/master/nls/en_US.UTF-8/Compose.pre"><code>xorg-libX11</code> repository</a>.
                On a live Linux system it's typically located at <code>/usr/share/X11/locale/en_us.UTF-8/Compose</code>.

            <p>MacOS does not provide equivalent functionality out of the box. The closest solution is the US International keyboard layout. However, it is exclusive with your preferred keyboard layout. In my case, the Polish layout which produces language-specific letters by holding any of the {{ "Option"|kbd }} keys (itself a downgrade from Linux or Windows where the <em>right</em> Alt key is required, and shortcuts using the left key do not conflict).
            </p>

            <p>You can always create your own, dedicated keyboard layout with apps like Ukelele. These <strong>still</strong> won't give you the same power as the Compose key solution.</p>
            <p>MacOS, however, has long since had this hidden remapping in the Cocoa text entry system, that we can use to replicate this behavior. There are several parts to that:
                <ol>
                    <li>Picking a key to serve as the Compose key.</li>
                    <li>If that's ordinarily considered a modifier key, remapping it to a non-modifier key the Cocoa text system can map.</li>
                    <li>Installing a mapping with sequences starting from that non-modifier key, which output desired characters.</li>
                </ol>
            </p>
            <p>There is one important difference: in Linux, a wrong compose sequence is simply aborted. No text is output. In MacOS, the wrong sequence will still output the last <em>unmatched</em> character, possibly leaving garbage. For example, the sequence for <code>¼</code> is <nowrap><kbd>Compose</kbd> <kbd>1</kbd> <kbd>4</kbd></nowrap>. If your fingers slip and you press <kbd>R</kbd> instead of <kbd>4</kbd>, the letter will be output (but not the preceding digit). </p>
            <h2>Opinionated choice</h2>
            <p>This generator helps you with these steps. The prefix key is fixed to {{ virtual_key|kbd }}, which is most likely not present on your keyboard. You can, choose the actual Compose key from a set of modifier keys that are present, and remap it to {{ virtual_key|kbd }}.</p>
            <p>Unlike Linux compose, the <code>dead_greek</code> key, used for producing Greek characters when followed by their Latin equivalents, is not mapped to a single key, but to <nowrap><kbd>Compose</kbd> <kbd>\</kbd></nowrap> (a lambda <em>λ</em> if you squint hard enough). Similarly, <code>dead_currency</code> is <span style="white-space: nowrap"><kbd>Compose</kbd> <kbd>$</kbd></span>.</p>
            <p>If that does not suit your circumstances, feel free to modify the code and use it on the command line to generate more presonalized mappings.</p>
        </section>
        <section id="generate-compose">
            <h2>1. Generate a <code>DefaultKeyBinding.plist</code> from Compose directives</h2>
            <form action="/convert" method="POST" encoding="multipart/form-data">
                <fieldset>
                    <legend>Choose your keyboard layout</legend>
                    {% for layout in layouts %}
                    <label>
                        <input type="radio" name="layout"
                               value="{{ layout }}"
                               checked="{{ 'checked' if default_layout == layout }}" /> {{ layout }}
                    </label>
                    {% endfor %}
                    <small>Not enough layouts? Create more and submit a PR!</small>
                </fieldset>
                <div class="grid">
                    <fieldset>
                        <legend>
                            Select symbol groups to include<br/>
                        </legend>
                        <div style="display:flex; flex-direction: column; flex-wrap: wrap">
                            {% for blkname in blocks %}
                            <label>
                                <input type="checkbox" name="include_blocks" value="{{ blkname }}"
                                    v-effect='$el.checked = included_blocks.indexOf({{ blkname|tojson }}) != -1'
                                    @change="update_blocks" />{{ blkname }}
                            </label>
                            {% endfor %}
                            <small>Note that checking <strong>all</strong> <em>is not</em> the same as checking all the other checkboxes,
                                as it contains some uncategorized symbols.</small>
                        </div>
                    </fieldset>
                    <fieldset id="exclude" :hidden="!showExcludeBlocks()">
                        <legend>Select symbol groups to <strong>exclude</strong></legend>
                        {% for blkname in blocks if blkname != 'all' %}
                        <label>
                            <input type="checkbox" name="exclude_blocks" value="{{ blkname }}" />{{ blkname }}
                        </label>
                        {% endfor %}
                    </fieldset>
                </div>
                <section class="grid">
                    <button type="submit" name="mode" value="generate">Generate</button>
                    <button type="submit" name="mode" value="preview" class="secondary">Preview</button>
                </section>
                <p>Save the resulting file as <code>~/Library/Keybindings/DefaultKeyBinding.dict</code> (create the directory if it doesn't exist yet). When updating this file, it's best to log out and back in, or restart your computer.</p>
            </form>

            <h2>2. Remap a preferred key to Compose on boot</h2>
            <form action="/remap" method="POST" encoding="multipart/form-data">
                <fieldset>
                    <legend>
                        Pick a single key to remap into Compose on boot.
                    </legend>
                    <small>If you're using <a href="https://karabiner-elements.pqrs.org/">Karabiner</a>, you may not need this step. Instead, create a mapping from your preferred key to {{ virtual_key|kbd }} in config. If that's not possible, remember to keep the key unbound.</small>
                    <label><input type="radio" name="compose_key" value="caps_lock"/> CapsLock </label>
                    <label><input type="radio" name="compose_key" value="left_ctrl"/> Left Control (^)</label>
                    <label><input type="radio" name="compose_key" value="left_gui"/> Left GUI (⌘) </label>
                    <label><input type="radio" name="compose_key" value="left_alt"/> Left Alt (⌥) </label>
                    <label><input type="radio" name="compose_key" value="right_alt"/> Right Alt (⌥) </label>
                    <label><input type="radio" name="compose_key" value="right_gui" checked /> Right GUI (⌘)</label>
                    <label><input type="radio" name="compose_key" value="application" /> Menu (≣)</label>
                    <label><input type="radio" name="compose_key" value="right_ctrl"/> Right Control (^) </label>
                </fieldset>
                <section class="grid">
                    <button type="submit" name="mode" value="generate">Generate</button>
                    <button type="submit" name="mode" value="preview" class="secondary">Preview</button>
                </section>
                <ol>
                    <li>Save the resulting file in <code>~/Library/LaunchAgents/dev.local.keyRemapper.plist</code></li>
                    <li>Log out and back in, or restart your computer. Do this every time you update this file.</li>
                </ol>
            </form>
            <h2>3. Disable the pop-up with accented letters</h2>
            <p>In the terminal, execute the following command:</p>
            <code style="display: block"> defaults write -g ApplePressAndHoldEnabled -bool false </code>
            <p>Then log out and back in, or restart your computer.</p>
            <em>Note: you don't need to restart three times - once after all three steps is fine.</em>
        </section>
    </main>
    {% include 'footer.html' %}
    <script type="module">
      import { createApp } from 'https://unpkg.com/petite-vue?module'

     createApp({
         included_blocks: ['all'],
         excluded_blocks: [],
         showExcludeBlocks() {
             console.table(this.included_blocks);
             return this.included_blocks.indexOf('all') != -1
         },
         mounted() {
             console.log('mount')
         },
         update_blocks(event) {
             // Can't use a set directly
             const incl = new Set(this.included_blocks)
             if (event.target.checked) {
                 incl.add(event.target.value)
                 this.included_blocks = [...incl.keys()]
             } else {
                 incl.delete(event.target.value)
                 this.included_blocks = [...incl.keys()]
             }
             console.log(this.included_blocks)
         }
     }).mount()
    </script>
</body>
</html>
