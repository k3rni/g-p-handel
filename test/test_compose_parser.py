from textwrap import dedent
from io import StringIO

from app.compose_parser import ComposeParser

def make_input(content: str):
    return StringIO(dedent(content))

def test_parses_valid_directives_and_comments():
    io = make_input('''\
    # Circled numbers
    <Multi_key> <parenleft> <KP_2> <KP_0> <parenright>	: "⑳"	U2473 # CIRCLED NUMBER TWENTY
    XCOMM KHMER DIGRAPHS
    <U17fd> : "េះ"
    /* The next two somehow don't work.  However, no extant APL uses "⍑". */
    <Multi_key> <macron> <U22a4>		: "⍑"	U2351 # ¯ ⊤ APL FUNCTIONAL SYMBOL UP TACK OVERBAR
    ''')

    # Keep only valid directives
    result = list(filter(None, ComposeParser().load(io)))
    assert len(result) == 3


