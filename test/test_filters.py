import pytest
from unicodedata import name as uname

from app import filters
from app.compose_parser import ComposeDirective

def cd(symbol: str):
    return ComposeDirective([], symbol, None, uname(symbol))

@pytest.mark.parametrize(
    'name,valid_symbols,invalid_symbols',
    [
        # GREEK CAPITAL LETTER ALPHA, GREEK SMALL LETTER MU, GREEK CAPITAL LETTER P, GREEK SMALL LETTER LAMBDA
        # vs
        # LATIN CAPITAL LETTER A, MICRO SIGN, CYRILLIC CAPITAL LETTER P, CARET INSERTION POINT
        ('filter_greek', 'Α μ Π λ', 'A µ П ⁁'),
        # LATIN SUBSCRIPT SMALL LETTER SCHWA
        ('filter_latin', 'a A ą Ą á Á ä Ä ǎ Ǎ æ Æ ₔ',  'ⓚ Я & 걻 🖖'),
        # CIRCLED LATIN CAPITAL LETTER K, CIRCLED DIGIT 1, CIRCLED NUMBER ELEVEN, CIRCLED PLUS, RIGHTWARDS BLACK CIRCLED WHITE ARROW,
        # CIRCLED HANGUL IEUNG U, CIRCLED IDEOGRAPH MOON, NEGATIVE CIRCLED CAPITAL LETTER S
        ('filter_circled', 'Ⓚ ① ⑪ ⊕ ⮊ ㉾ ㊊ 🅢', 'K 1'),
        # EURO SIGN, WON SIGN, BAHT SIGN
        # vs
        # DOLLAR SIGN, CURRENCY SIGN, CENT SIGN
        # XXX: Maybe should be added to special cases list
        ('filter_currency', '€ ₩ ₿', '$ ¤ ¢'),
        # ASTERISK OPERATOR, FOR ALL, ELEMENT OF, N-ARY PRODUCT, CUBE ROOT, PLACE OF INTEREST SIGN, APL FUNCTIONAL SYMBOL TILDE DIAERESIS
        # vs
        # PLUS SIGN, ASTERISK, CYRILLIC LETTER UKRAINIAN IE
        ('filter_math', '∗ ∀ ∈ ∏ ∛ ⌘ ⍨', '+ * Є'),
        # LEFT DOUBLE QUOTATION MARK, SINGLE LOW-9 QUOTATION MARK QUOTATION MARK, COMMA, ASTERISK
        # This is a unicodeblocks feature - they're listed in a block named BASIC_PUNCTUATION which is not a unicode thing
        ('filter_punctuation', '“ ‚ " , *', 'A 1 Г'),
        # SUPERSCRIPT ONE, SUPERSCRIPT ZERO, SUPERSCRIPT LEFT PARENTHESIS, LATIN SUBSCRIPT SMALL LETTER SCHWA vs DEGREE SIGN
        ('filter_superscripts_and_subscripts', '¹ ⁰ ⁽ ₔ', '°'),
        # fractions vs FRACTION SLASH
        ('filter_fractions', '½ ¼ ¾ ⅜ ⅒', '⁄')

    ]
)
def test_filter(name, valid_symbols, invalid_symbols):
    filter = getattr(filters, name)
    for sym in valid_symbols.split():
        assert filter(cd(sym)), f"{sym}(U+{ord(sym):04x}, {uname(sym)}) should pass {name}"

    for sym in invalid_symbols.split():
        assert not filter(cd(sym)), f"{sym}(U+{ord(sym):04x}, {uname(sym)}) should not pass {name}"
