from app.converter import *
from app.compose_parser import ComposeDirective

def cd(symbol: str):
    return ComposeDirective([], symbol, None, uname(symbol))

def test_available_layouts():
    layouts = available_layouts()
    assert len(layouts) == 1
    assert layouts[0] == 'US'

def test_lookup_layout():
    layout = lookup_layout('US')
    assert layout is not None
    assert layout.NAMED_KEYS['ampersand'] == '$7'

def test_available_blocks():
    blocks = available_blocks()
    assert len(blocks) > 0
    assert 'all' in blocks
    assert 'latin' in blocks

def test_select_filters():
    filters = select_filters(('latin', 'asian', 'punctuation'))
    assert len(filters) == 2
    assert all(callable(f) for f in filters)

def test_build_filter_inclusive():
    func = build_filter(('latin', 'asian', 'punctuation'), ())
    assert callable(func)
    assert func(cd('A'))
    assert func(cd(','))
    assert not func(cd('Я'))

def test_build_filter_exclusive():
    func = build_filter(('all',), ('punctuation'))
    assert callable(func)
    assert func(cd('A'))
    assert not func(cd(','))
    assert func(cd('Я'))
