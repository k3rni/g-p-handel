from flask import Flask, render_template, request
from markupsafe import Markup

from app.converter import available_layouts, available_blocks, Converter
from app.remapper import Remap
from io import StringIO

app = Flask(__name__)

VIRTUAL_KEY = "F16"

def default_compose_file():
    return open('Compose', 'r')

@app.template_filter('kbd')
def kbd(text):
    return Markup('<kbd>%s</kbd>') % (text,)

@app.template_filter('code')
def code(text: str):
    return Markup('<code>%s</code>') % (text,)

@app.route("/")
def index():
    return render_template(
        "index.html",
        layouts=available_layouts(),
        default_layout=available_layouts()[0],
        blocks=available_blocks(),
        virtual_key=VIRTUAL_KEY,
        comp=kbd("Compose")
    )

@app.route("/convert", methods=('POST',))
def convert():
    form = request.form
    converter = Converter(
        form['layout'],
        set(form.getlist('include_blocks')),
        set(form.getlist('exclude_blocks')),
        VIRTUAL_KEY
    )
    buf = StringIO()
    converter.convert(default_compose_file(), buf)
    headers = {'Content-Type': 'text/plain; charset=utf-8'}
    if form.get('mode') == 'generate': # As opposed to preview
        headers['Content-Disposition'] = 'attachment; filename=DefaultKeyBinding.dict'
    return (buf.getvalue(), headers)

@app.route("/remap", methods=('POST',))
def remap():
    form = request.form
    remapper = Remap(form['compose_key'], VIRTUAL_KEY)
    buf = StringIO()
    remapper.remap(buf)
    headers = {'Content-Type': 'text/plain; charset=utf-8'}
    if form.get('mode') == 'generate': # As opposed to preview
        headers['Content-Disposition'] = 'attachment; filename=dev.local.keyRemapper.plist'
    return (buf.getvalue(), headers)
